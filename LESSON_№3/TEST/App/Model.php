<?php
namespace App;

abstract class Model
{
  const TABLE = '';
  public $id;

  public static function findAll()
  {
    $db = Db::instance();
    $sql = 'SELECT * FROM ' . static::TABLE;
    $class = static::class;
    return $db->query($sql, [], $class);
  }

  public function isNew()
  {
    return empty($this->id);
  }

  public function insert()
  {
    if (!$this->isNew()) {
      return;
    }

    $columns = [];
    $values = [];
    foreach ($this as $key => $value) {
      if ('id' == $key) {
        continue;
      }
      $columns[] = $key;
      $values[':' . $key] = $value;
    }
    $sql = 'INSERT INTO ' . static::TABLE . '
    ('. implode(",", $columns) .')
    VALUES
    ('. implode(",", array_keys($values)) .')
    ';

    $db = Db::instance();
    $db->execute($sql, $values);
    $this->id = $db->getLastId();
  }
}
