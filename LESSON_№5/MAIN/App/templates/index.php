<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Новости Ставрополя!!!</title>
  </head>
  <body>
    <h1>Новости!!!</h1>

    <?php foreach ($this->articles as $article): ?>
      <article>
        <h2>
          <a href="./?ctrl=Article&id=<?php echo $article->id; ?>">
            <?php echo $article->title; ?>
          </a>
        </h2>
        <p><?php echo $article->content; ?></p>
      </article>
      <hr>
    <?php endforeach; ?>

  </body>
</html>
