<?php
namespace App\Controllers;

use App\Controller;
use App\Models\Article;

class Index extends Controller
{
  protected function handle()
  {

    $articles = Article::findAll();
    //$view->assign('articles', $articles);
    $this->view->articles = $articles;

    $this->view->display('index.php');
  }
}
