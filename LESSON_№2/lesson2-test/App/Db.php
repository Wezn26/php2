<?php
namespace App;

use PDO;

class Db
{
  use Singleton;
  protected $dbh;

  protected function __construct()
  {
    $config = require __DIR__ . './../config.php';
    $this->dbh = new PDO($config['dsn'], $config['username'], $config['password']);
  }

  public function execute(string $sql, array $params = [])
  {
    $sth = $this->dbh->prepare($sql);
    $res = $sth->execute($params);
    return $res;
  }

  public function query(string $sql,array $params = [], string $class)
  {
    $sth = $this->dbh->prepare($sql);
    $res = $sth->execute($params);
    if (false !== $res) {
      return $sth->fetchAll(PDO::FETCH_CLASS, $class);
    } else {
      return [];
    }

  }

  public function getLastId()
  {
    return $this->dbh->lastInsertId();
  }
}
