<?php

require __DIR__ . '/autoload.php';

$article = \App\Models\Article::findById($_GET['id']);

if (isset($_GET['id'])) {
    $article = \App\Models\Article::findById($_GET['id']);
    if (false === $article) {
        header('Location: /');
        exit();
    }
}

include __DIR__ . '/App/Templates/article.php';