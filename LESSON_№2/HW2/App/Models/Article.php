<?php


namespace App\Models;


use App\Classes\AbstractModel;
use App\Classes\Db;

class Article extends AbstractModel
{
    protected const TABLE = 'news';

    public $id;
    public $title;
    public $content;

    public static function LastThreeNews()
    {
        $sql = 'SELECT * FROM news ORDER BY id DESC LIMIT 3';
        $db = new Db();
        return $db->query($sql);
    }
}