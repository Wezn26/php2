<?php

namespace App\View;

trait Singleton
{
  protected static $instance;
  protected function __construct()
  {
  }
  public static function instance()
  {
    if (null === static::$instance) {
      $obj = static::$instance = new static;
    }
    return $obj;
  }
}
