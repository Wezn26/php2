<?php

namespace App\View;

use PDO;

class Db
{

  use Singleton;
  protected $dbh;
  protected function __construct()
  {
    $config = require __DIR__ . './../config.php';
    $this->dbh = new PDO($config['dsn'], $config['user'], $config['password']);
  }

  public function getQuery(string $sql,  array $data = [])
  {
    $sth = $this->dbh->prepare($sql);
    $res = $sth->execute($data);
    return $res;
  }

  public function query($class, string $sql, array $data = [])
  {
    $sth = $this->dbh->prepare($sql);
    $res = $sth->execute($data);
    if (false !== $res) {
      $all = $sth->fetchAll(PDO::FETCH_CLASS, $class);
      return $all;
    } else {
      die('ERROR!!!Datebase not connected!!!');
    }
  }
}
