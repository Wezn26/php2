<?php

namespace App\View;

class View
implements \Countable
{
  protected $data = [];

  public function __set($name, $value)
  {
    $this->data[$name] = $value;
  }

  public function __get($name)
  {
    return $this->data[$name];
  }

  public function __isset($name)
  {
    return isset($this->data[$name]);
  }

  public function render($template)
  {
    ob_start();
    foreach ($this->data as $prop => $value) {
      $$prop = $value;
    }
    include __DIR__ . '/../templates/' . $template;
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
  }

  public function display($template)
  {
    echo $this->render($template);
  }

  /**
   * Count elements of an object
   * @link http://php.net/manual/en/countable.count.php
   * @return int The custom count as an integer.
   * </p>
   * <p>
   * The return value is cast to an integer.
   * @since 5.1.0
   */
  public function count()
  {
    return count($this->data);
  }
}
