<?php 
function autoload($class)
{
  $file = __DIR__ . '/../' . str_replace('\\', '/', $class) . '.php';
  if (file_exists($file)) {
    require $file;
  } else {
    echo 'ERROR!!!AutoloadFailure!!!';
  }
}
spl_autoload_register('autoload');