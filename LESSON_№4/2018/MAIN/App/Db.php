<?php
namespace App;
use PDO;

class Db
{
  use Singleton;
  protected $dbh;

  protected function __construct()
  {
    $config = require __DIR__ . '/config.php';
    $this->dbh = new PDO($config['dsn'], $config['user'], $config['password']);
  }

  public function execute(string $sql, $data)
  {
    $sth = $this->dbh->prepare($sql);
    $res = $sth->execute($data);
    return $res;
  }

  public function query(string $sql, array $data = [], $class)
  {
    $sth = $this->dbh->prepare($sql);
    $res = $sth->execute($data);
    if (false !== $res) {
      return $sth->fetchAll(PDO::FETCH_CLASS, $class);
    }
    echo "ERROR!!!Query Failure!!!";
  }

  public function getLastId()
  {
    return $this->dbh->lastInsertId();
  }
}
