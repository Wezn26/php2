<?php
namespace App\Controllers;

use App\Controller;
use App\View;



class Article extends Controller
{
  // protected function access() : bool
  // {
  //   return isset($_GET['name']) && 'Vasya' == $_GET['name'];
  // }
  protected function handle()
  {
    $article = \App\Models\Article::findById($_GET['id']);
    //$view->assign('articles', $articles);
    $this->view->article = $article;

    $this->view->display('article.php');
  }
}
