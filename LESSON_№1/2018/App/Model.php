<?php 
namespace App;

abstract class Model
{
  public $id;
  public const TABLE = ''; 
 

  public static function findAll()
  {
    $db = new Db;    
    $sql = 'SELECT * FROM ' . static::TABLE;
    $article =  static::class;
    return $db->query($sql, [], $article);
  }
}