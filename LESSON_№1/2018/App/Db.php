<?php 
namespace App;
use PDO;
class Db
{
  protected $dbh;
  public function __construct()
  {
    $config = require __DIR__ . './../config.php';
    $this->dbh = new PDO($config['dsn'], $config['username'], $config['password']);
  }

  public function query(string $sql, array $data, $class)
  {
    $sth = $this->dbh->prepare($sql);
    $sth->execute($data);
    return $sth->fetchAll(PDO::FETCH_CLASS, $class);
  }

  public function badquery(string $sql, array $data = [], $class)
  {
    $sth = $this->dbh->prepare($sql);
    $sth->execute($data);
    $data = $sth->fetchAll();
    $ret = [];
    foreach ($data as $row) {
      $item = new $class;
      foreach ($row as $key => $value) {
        if (is_numeric($key)) {
          continue;
        }
        $item->$key = $value;
      }
      $ret[] = $item;
    }
    return $ret;
  }
}