CREATE TABLE news(
  id SERIAL,
  title VARCHAR(100) NOT NULL,
  content TEXT
);

INSERT INTO news
(title, content)
VALUES
('Формула 1 в Ставрополе!!!',
'Гран-при Ставрополя состоялся, сегодня 21 июля 2121 года!!!');

INSERT INTO news
(title, content)
VALUES
('Прямые рейсы на Марс с 1 декабря 2121 года',
'Ставропольчане теперь могут летать на Марс без пересадок!!!');


CREATE TABLE users(
  id SERIAL,
  email VARCHAR(255),
  name VARCHAR(100)
);

INSERT INTO users
(email, name)
VALUES
('test@test.com', 'Ivan');

INSERT INTO users
(email, name)
VALUES
('test@test.ru', 'Petrov');